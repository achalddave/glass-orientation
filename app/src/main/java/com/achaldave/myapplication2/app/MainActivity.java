package com.achaldave.myapplication2.app;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Queue;

public class MainActivity extends ActionBarActivity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mSensor;
    /* Not proud of this global, but hard to make it cleaner. */
    private Orientation orientation;
    private OrientationHistory previousOrientations;
    /* Sorted list of rotation matrices at which something was tagged. */
    private LocationMap locations;

    /* Next id of device. */
    private int nextId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locations = new LocationMap();
        orientation = new Orientation();
        previousOrientations = new OrientationHistory(10);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        // Get an instance of the SensorManager
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        // Get updates every 10ms.
        mSensorManager.registerListener(this, mSensor, 10000);

        super.onStart();
    }

    @Override
    public void onStop() {
        // make sure to turn our sensor off when the activity is paused
        mSensorManager.unregisterListener(this);

        super.onStop();
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            float[] tmp = new float[16];
            float[] remapped = new float[16];
            float[] rawOrientation = new float[3];
            SensorManager.getRotationMatrixFromVector(tmp, event.values);
            SensorManager.remapCoordinateSystem(tmp, SensorManager.AXIS_X, SensorManager.AXIS_Z, remapped);
            SensorManager.getOrientation(remapped, rawOrientation);

            orientation.setOrientation(rawOrientation[1], rawOrientation[0]);
            previousOrientations.add(new Orientation(orientation));
            int match = locations.getMatch(previousOrientations);

            if (match >= 0) {
                ((TextView) findViewById(R.id.found)).setText(String.format("Found: %d", match));
                Location found = locations.getById(match);
            } else {
                ((TextView) findViewById(R.id.found)).setText("...");
            }

            if (previousOrientations.hasMovement())
                ((TextView) findViewById(R.id.movement)).setText("You're moving!");
            else
                ((TextView) findViewById(R.id.movement)).setText("...");
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        /* Don't really care about this. */
    }

    public void save(Location updated) {
        Log.d("Finder", "Saving location for " + Integer.toString(updated.id));
        locations.add(new Location(updated));
    }

    public void updateMap(Location updated) {
        int id = updated.id;
        // Log.d("Finder", "Updating location for " + Integer.toString(id));
        locations.update(updated);
    }

    /**
     * Update the location of @ids such that they are centered around @orientation
     *
     * TODO: Ideally, we get the center point of @ids, and update all the devices
     *       such that the center would be @orientation.
     *
     * @param ids              The devices to update
     * @param orientation      Where the user is looking
     */
    public void updateMap(ArrayList<Integer> ids, Orientation orientation) {
        locations.update(ids, orientation);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_S:
                Log.d("Key", "S");
                locations.saveMap(this);
                return true;
            case KeyEvent.KEYCODE_L:
                Log.d("Key", "L");
                locations.loadMap(this);
                return true;
            case KeyEvent.KEYCODE_1:
                Log.d("Key", "1");
                save(new Location(1, orientation));
                return true;
            case KeyEvent.KEYCODE_2:
                Log.d("Key", "2");
                save(new Location(2, orientation));
                return true;
            case KeyEvent.KEYCODE_3:
                Log.d("Key", "3");
                save(new Location(3, orientation));
                return true;
            case KeyEvent.KEYCODE_4:
                Log.d("Key", "4");
                save(new Location(4, orientation));
                return true;
            case KeyEvent.KEYCODE_5:
                Log.d("Key", "5");
                save(new Location(5, orientation));
                return true;
            case KeyEvent.KEYCODE_Q:
                Log.d("Key", "Q");
                locations.setLit(locations.getById(1), orientation);
                return true;
            case KeyEvent.KEYCODE_W:
                Log.d("Key", "W");
                locations.setLit(locations.getById(2), orientation);
                return true;
            case KeyEvent.KEYCODE_E:
                Log.d("Key", "E");
                locations.setLit(locations.getById(3), orientation);
                return true;
            case KeyEvent.KEYCODE_R:
                Log.d("Key", "R");
                locations.setLit(locations.getById(4), orientation);
                return true;
            case KeyEvent.KEYCODE_T:
                Log.d("Key", "T");
                locations.setLit(locations.getById(5), orientation);
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
