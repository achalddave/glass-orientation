package com.achaldave.myapplication2.app;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

/**
 * Created by Achal on 4/7/14.
 */
public class LocationMap {
    private ArrayList<Location> sortedLocations;
    private Location litLoc;
    private Orientation litOrientation;
    /* Threshold at which we decide to switch to another device. */
    private static final float SWITCH_THRESHOLD = (float) 0.1;

    public LocationMap() {
        sortedLocations = new ArrayList<Location>();
    }

    public Location getById(int id) {
        int idx = getIdxById(id);
        if (idx < 0) return null;
        return getByIndex(getIdxById(id));
    }

    public int getIdxById(int id) {
        for (int i = 0; i < sortedLocations.size(); ++i) {
            if (sortedLocations.get(i).id == id)
                return i;
        }
        return -1;
    }
    public Location getByIndex(int idx) {
        if (sortedLocations.size() == 0) return null;
        return sortedLocations.get(idx);
    }

    public void add(Location location) {
        if (sortedLocations.size() > 0) {
            for (Location loc : sortedLocations) {
                Orientation.Direction dir = loc.orientation.getDirection(location.orientation);
                Log.d("Location", String.format("%s to %s of %s", location.orientation, dir, loc.orientation));
            }
        }
        int idx = getIdxById(location.id);
        if (idx < 0) {
            sortedLocations.add(location);
        } else {
            sortedLocations.set(idx, location);
        }
        sortLocations();
    }

    public void update(Location updated) {
        Location old = getById(updated.id);
        if (old == null) {
            add(updated);
            return;
        }
        Orientation oldOrientation = new Orientation(old.orientation);
        for (Location loc : sortedLocations) {
            if (loc.id == updated.id) {
                loc.setOrientation(updated.orientation);
                continue;
            }

            /* currNew = new + (currOld - old); */
            Orientation currUpdated = updated.orientation.add(loc.orientation.sub(oldOrientation));
            Log.d("Updater", String.format("%s -> %s, because %s->%s",
                    loc.orientation, currUpdated, oldOrientation, updated.orientation));
            loc.setOrientation(currUpdated);
        }
        sortLocations();
    }

    /**
     * Update @updated devices so that their average falls on the new @average.
     *
     * @param updated Devices that have been updated.
     * @param average New average orientation of those devices.
     */
    public void update(ArrayList<Integer> updated, Orientation average) {
        ArrayList<Orientation> oldOrientations = new ArrayList<Orientation>();
        for (int id : updated) {
            oldOrientations.add(getById(id).orientation);
        }
        Orientation oldAverage = Orientation.getAverage(oldOrientations);
        oldAverage.div(updated.size());

        /* Update the first device, then use that to update the rest. */
        Location first = getById(updated.get(0));
        Location firstNew = new Location(first);
        /* new = newAvg + (old - oldAvg) */
        firstNew.orientation = average.add(first.orientation.sub(oldAverage));

        /* Update the rest of the devices; if we assume the location of any one device, the rest
         * will follow. */
        update(firstNew);
     }

    /**
     * @param litLoc What IR has lit
     * @param litOrientation The orientation when IR started lighting basLoc
     */
    public void setLit(Location litLoc, Orientation litOrientation) {
        Log.d("Finder", String.format("Setting litLoc to %d", litLoc.id));
        Log.d("Finder", String.format("Setting lit as %d; curr: %s, litLoc.orientation: %s", litLoc.id, litLoc.orientation, litOrientation));
        this.litLoc = litLoc;
        this.litOrientation = new Orientation(litOrientation);
    }

    public int getMatch(OrientationHistory orientations) {
        if (sortedLocations.size() < 1) return -1;
        if (litLoc == null) return -1;
        // Get the most recent orientation relative to the orientation when current device was lit.
        Orientation rawOrientation = orientations.get(-1);
        /* Get difference between rawOrientation and litOrientation: this is how much we've moved.
         * Then, add that difference to the original location of the lit device.
         */
        Orientation orientation = litLoc.orientation.add(rawOrientation.sub(litOrientation));

        if (litLoc.orientation.distance(orientation) < SWITCH_THRESHOLD) return litLoc.id;

        Orientation.Direction dir = litLoc.orientation.getDirection(orientation);
        Log.d("Finder", String.format("Are ***%s*** relative to %d.", dir, litLoc.id));

        double bestDistance = Float.MAX_VALUE;
        int closestId = -1;
        for (int i = 0; i < sortedLocations.size(); ++i) {
            Location loc = sortedLocations.get(i);
            if (loc.id == litLoc.id) continue;
            Orientation.Direction currDir = litLoc.orientation.getDirection(loc.orientation);
            Log.d("Finder", String.format("%d is ***%s*** relative to %d.", loc.id, currDir, litLoc.id));
            if (currDir != dir) {
                continue;
            }

            double currDistance = loc.orientation.distance(litLoc.orientation);
            if (currDistance < bestDistance) {
                bestDistance = currDistance;
                closestId = loc.id;
            }
        }
        // if (bestDistance > 0.1) return -1;
        return closestId;
    }

    public void sortLocations() {
        Collections.sort(sortedLocations);
    }

    public void saveMap(Activity ctx) {
        SharedPreferences prefs = ctx.getPreferences(ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        for (Location loc : sortedLocations) {
            editor.putString(Integer.toString(loc.id), loc.orientation.serialize());
        }
        editor.commit();
    }

    public void loadMap(Activity ctx) {
        SharedPreferences prefs = ctx.getPreferences(ctx.MODE_PRIVATE);
        Map<String,?> locations = prefs.getAll();
        if (locations.size() <= 0) return;
        for (Map.Entry<String, ?> locationStr : locations.entrySet()) {
            int id = Integer.parseInt(locationStr.getKey());
            Orientation orientation = new Orientation(locationStr.getValue().toString());
            add(new Location(id, orientation));
        }
    }
}
